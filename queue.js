let collection = [];
let first = 0;
let last = collection.length - 1


// Write the queue functions below.

// 1. Output all the elements of the queue
function print() {
  return collection
}

// 2. Adds element to the rear of the queue
function enqueue(element) {
  collection = collection.concat(element);
  return collection
}

// 3. Removes element from the front of the queue
function dequeue() {
  let dequeuedCollection = [];
  for(let i = 1; i < collection.length; i++) {
    collection = dequeuedCollection.concat(collection[i])
    return collection
  }
}

// 4. Show element at the front
function front() {
  return collection[first]

}

// 5. Show the total number of elements
function size() {
  return collection.length;

}

// 6. Outputs a Boolean value describing whether queue is empty or not
function isEmpty() {
  return collection.length === 0 ;
}


module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};
